# systranio

[![View pipeline status](https://gitlab.com/canarduck/systranio/badges/master/pipeline.svg)](https://gitlab.com/canarduck/systranio/commits/master) [![See coverage report in HTML](https://gitlab.com/canarduck/systranio/badges/master/coverage.svg)](https://canarduck.gitlab.io/systranio/coverage_html_report)
[![Get current version](https://img.shields.io/pypi/v/systranio.svg)](https://pypi.org/project/systranio/)

An *unofficial* python3 client to Systran.io APIs, [official libraries](https://github.com/SYSTRAN/translation-api-python-client) are also available on Systran repositories.

* Documentation : https://canarduck.gitlab.io/systranio
* Repository : https://gitlab.com/canarduck/systranio
* Pypi : https://pypi.org/project/systranio