# Welcome to systranio's documentation

[![View pipeline status](https://gitlab.com/canarduck/systranio/badges/master/pipeline.svg)](https://gitlab.com/canarduck/systranio/commits/master) [![See coverage report in HTML](https://gitlab.com/canarduck/systranio/badges/master/coverage.svg)](https://canarduck.gitlab.io/systranio/coverage_html_report)
[![Get current version](https://img.shields.io/pypi/v/systranio.svg)](https://pypi.org/project/systranio/)

An *unofficial* python3 client to Systran.io APIs, [official libraries](https://github.com/SYSTRAN/translation-api-python-client) are also available on Systran repositories..

## Quickstart

1. Register & get an API key on [systran.io](https://platform.systran.net)
2. `pip install systranio`
3. Code sample :

```
import systranio

translation = systranio.Translation(YOUR_API_KEY)
options = {'source': 'en' } 
result = translation.text('translation', 'fr', **options)
print(result)  # traduction
```

## APIs

Systranio is still a work in progress. The goal is to provide a fully working Translation API (text & file) with version 1.0.0.
See the [changelog](https://gitlab.com/canarduck/systranio/blob/master/CHANGELOG.md) for more detailed progress report

### Translation API

* *Status* : WIP
* *Documentation* : [Translation API](translation.md)
* *Available calls* :

    * [x] Translate text
    * [x] Translate file
    * [x] Get translation status
    * [x] Cancel translation
    * [x] Get translation result
    * [x] Supported languages
    * [x] Supported formats
    * [x] Get API version
    * [x] List profiles 

### Natural Language Processing

Status : _not implemented_

### Ressource Management

Status : _not implemented_

### Multimodal Text Extraction

Status : _not implemented_